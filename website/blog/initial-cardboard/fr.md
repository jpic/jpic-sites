# Début d'un site de cadres en carton art déco

Vous pouvez voir la version initiale d'un [site de cadres en carton d'une amie
artiste](http://cardboard.james.pm). Vous conviendrez qu'une photo de 500px de
large ne suffit pas. Alors je lui ai envoyé un email et elle va s'occupper de
faire de meilleures photos. En 5 minutes de travail pour commiter et pusher une
image et une poignée de titres le projet a commencé. Nous verrons comment
l'experience contiue !
