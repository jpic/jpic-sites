# Starting a cardboard art website

You can see the initial version of the [website for cardboard frame artist
friend](http://cardboard.james.pm). You can quickly notice that 500px wide
pictures don't render nicely, I've mailed her about it and she's now working on
making better pictures. In only 5 minutes of work needed to commit a picture
and a handful of titles I already got it started. We'll see how the experiment
turns out !
