Menu-title: What ?
Description: A multi-site simple CMS system for hackers, to hack on local
             economy using their brain and technical research.

# KISS Multi-Site CMS for Hackers
# Develop local economy
# Focus on what matters

Django CMS Light is a [prototype](https://gitlab.com/yourlabs/django-cms-light)
created for research on local growth and economy hacking. The purpose is to
help locals make a living of their passions. Long term engineering decisions
have been made in the rewrite branch which is only being tested automatically
so we're further from having anything visible. Nonetheless, nixcms will be
continued if doubts clear after I work with Pelican on the Memopol website
which strives to be something else than a blog, where I'd like to try to
implement a vision to bring Humans back at the center of internet. Pages should
not look like featurefull like there's something to sell because that's what we
use to do.

    James, at your service
