Description: Un CMS simple multi-site pour hackers, pour hacker l'économie
             locale en utilisant leur cerveau et apprentissage technique.
Menu-Title: Quoi ?
Url: qu-est-ce-que-james-cms

# CMS KISS multi site pour Hackers
# Developpez l'économie locale
# Concentrez-vous sur ce qui compte

Django CMS Light est un
[prototype](https://gitlab.com/yourlabs/django-cms-light) utilisé dans la
recherche sur le developpement de la croissance et de l'économie locale. Le but
est de contribuer à internet pour permettre aux gens de vivre leur passion. Des
décisions techniques long-termistes ont été pris dans la branche "rewrite" et
n'est testée qu'automatiquement donc aucun avancement n'est visible. Cependant,
nixcms sera peut-être continué après avoir construit une certitude à 4 chiffres
qu'on ne peut pas apporter tant de valeur avec un logiciel existant,
raffraichir le [Site du projet Memopol](http://www.memopol.org) qui est en
pelican avec un coup de gulp et bootstrap 4 en scss

   James, à votre service
