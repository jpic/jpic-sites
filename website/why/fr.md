Title: Un CMS multi-site simple pour tous
Url: pourquoi
Menu-title: Pourquoi ?
Description: Django CMS Light facilite la tâche aux hackers qui veulent aider
             leurs amis à faire des sites internet.

# Pourquoi Django CMS Light ?

Il y a déjà de nombreux CMS libres, alors pourquoi en créer un
nouveau ? Django CMS Light est une innovation qui sort complêtement
des sentiers battus comme vous pourrez le comprendre, en
reprenant les concepts de gestion de contenu en ligne à zéro.

## Etat des lieux

### Les services en ligne

Il y a de nombreux services en lignes tels que [Google
Sites](https://apps.google.fr/intx/fr/products/sites/), [Wix](http://wix.com),
[Facebook Pages](https://www.facebook.com/pages/create/),
[About.me](http://about.me). Mais ils sont payants et surtout non-libre. Vous
n'êtes pas [maître de vos données](https://indieweb.org/why), votre site ne
vous appartient pas et vous serez soumis aux limitations techniques de leurs
systèmes qui sont comparables aux systèmes LAMP classiques.

### La LAMP classique

En règle générale, voici comment la gestion de contenu fonctionne sur internet
avec des CMS classiques tels que [Wordpress](http://wordpress.com),
[Spip](http://www.spip.net) ou [Django-CMS](https://www.django-cms.org):

- on installe un CMS et puis on le configure,
- on créé des comptes pour gérer son contenu en base de données via des
  formulaires,
- on fork ou on fait un module pour ajouter ses contenus multimedias et ses
  feuilles de style CSS,
- on maintient une installation par site ou alors on essaye de faire marcher un
  plugin multi-sites,
- on fait des sauvegardes régulièrement ou on finit par pleurer sa maman.

### Les systèmes minimalistes

Quelques exceptions, telles que [Werc](http://werc.cat-v.org/),
[Jekyll](http://jekyllrb.com) et [Pelican](http://getpelican.com) commencent à
sortir un peu de ce schéma:

- on gère son contenu en fichiers,
- on utilise une commande pour générer des fichiers statiques,
- on héberge ceux-ci, par exemple sur [GitLab Pages](https://pages.gitlab.io/)
  ou [GitHub Pages](https://pages.github.com/) ou encore n'importe quel
  hebergeur de fichiers statiques,
- encore une fois, on a un hébergement par site à gerer.

## Principes de Django CMS Light

Django CMS Light a été créé dans le but de [rendre internet à l'Hummanité](/mission/)
et s'appui sur des concepts très différents:

- le contenu texte et multimédia et versionné avec les feuilles de styles sur
  [git](https://git-scm.com), on peut profiter d'un hébergeur gratuit et libre
  tel que [GitLab](https://gitlab.org) ou seulement gratuit tel que
  [GitHub](https://github.com), on a un site par branche,
- on gère une installation pour tous ses sites, je peux éventuellement ajouter
  votre site à mon installation, sinon demandez à votre hacker de proximité
  d'en héberger un quelque part, il peut rentrer en contact avec moi si il a
  besoin d'aide. Tout le monde est le bienvenu pour apprendre, l'élitisme n'a
  pas sa place dans notre communauté.
- une installation peut donc servir plusieurs sites utilisant plusieurs dépôts
  git, et ce n'est pas grave si son serveur est en panne, il suffit d'en
  remonter un et de provisionner sa base de données avec les dépôts gits, ou
  encore demander à un de ses amis qui a une instance de Django CMS Light d'ajouter von
  dépôts de sites et pointer vos noms de domaine dessus en attendant de réparer
  votre Django CMS Light ou d'en remonter un.

## Pourquoi Django CMS Light ?

Django CMS Light est le prénom du majordome, c'est à dire "James, faites moi un café je
vous prie". C'est pourquoi Django CMS Light est le nom de ce système qui est fait pour
tous ces petits sites que je n'ai pas eu le temps de faire pour mes amis.

Maintenant, tout ce que j'ai besoin de faire pour travailler sur le contenu de
mes sites et de ceux de mes amis c'est de pousser du contenu sur git.
