# Existing classic LAMP CMS

This is how content management works with classic CMS systems such as
[Wordpress](http://wordpress.com), [Spip](http://www.spip.net) or
[Django-CMS](https://www.django-cms.org):

- install the CMS somewhere and configure it,
- create accounts to manage the content database and files using web forms,
- fork or make a module to add your own static contents and CSS stylesheets,
- maintain one setup per website or try to make a multi-site system work,
- backup regularely or cry your mom one day,
