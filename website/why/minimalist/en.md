# Existing minimalist CMS systems

A few exceptions such as [Werc](http://werc.cat-v.org/),
[Jekyll](http://jekyllrb.com) or [Pelican](http://getpelican.com) change the
schema a bit:

- manage content as files,
- use a command to generate static HTML files,
- host them, ie. on [GitLab Pages](https://pages.gitlab.io/)
  or [GitHub Pages](https://pages.github.com/) or any web host.
- again, manage one hosting per website.

