Menu-title: Why ?

# Why invent a new CMS ?

Here are some of the studies that have been made before starting Django CMS Light CMS:

{% for child in children %}
- [{{ child.title }}]({{ child.url }})
{% endfor %}
