# Django CMS Light CMS principles

Django CMS Light CMS was created with the purpose of [giving internet back to
Humanity](/mission/) is built on different schemas:

- text and multimedia contents and stylesheets are versioned with
  [git](https://git-scm.com), you can host it on a free (as in free speech plus
  free beer) host such as [GitLab](https://gitlab.org), on on a free (as in
  free beer) system such as
  [GitHub](https://github.com), you can have multiple repositories, one branch
  per website,
- maintain only one Django CMS Light setup for all your websites. I can add your website
  to my own, but Django CMS Light is designed so that you can ask your "proximity hacker"
  to host a Django CMS Light somewhere on internet. They can ask me for help any time if
  they need any help: everybody is welcome to learn, elitism doesn't have its
  place in our community.
- one setup can serve many websites from many git repositories. It doesn't
  matter if your server crashes, because you can just spawn another one and
  provision it from your git repositories, and even ask a friend to add your
  repositories to his Django CMS Light instance and change your DNS to point there while
  you're fixing or reinstalling your new server.
