# Existing online hosted CMS

There is already plenty of online services such as [Google
Sites](https://apps.google.fr/intx/en/products/sites/), [Wix](http://wix.com),
[Facebook Pages](https://www.facebook.com/pages/create/),
[About.me](http://about.me). But they cost money and are not
[free](http://www.fsf.org/). You don't [own your
data](https://indieweb.org/why) and your website doesn't belong to you. Also,
it has the same technical limitations as classic LAMP systems.
