Title: Give internet back to Humanity
Menu-title: Mission
Description: Developper la communication entre les Humains du monde et
             leur rendre la proprieté sur leurs contenus.


Django CMS Light is a multi-site
[CMS](https://en.wikipedia.org/wiki/Content_management_system) using [git
repositories](http://git-scm.org) to provision its database, stylesheet and
static contents. You can see the [stylesheet used on this
page](https://gitlab.com/jpic/jpic-sites/blob/master/page.css), the [background
image](https://gitlab.com/jpic/jpic-sites/blob/master/background.jpg) displayed
in responsive here, [this
text](https://gitlab.com/jpic/jpic-sites/blob/master/en.md) as well as its
[french translation](https://gitlab.com/jpic/jpic-sites/blob/master/fr.md), and
the [metadata used for this
website](https://gitlab.com/jpic/jpic-sites/blob/master/site.yaml).

Instead of generating static files, it's a WSGI server in
[python](http://python.org) and [django](http://djangoproject.com) which clones
[git repositories](https://gitlab.com/jpic/jpic-sites) parses their structure
and content to provision its database and cache. You don't have to use any web
form and the database is completely volatile. It's like
[Pelican](http://getpelican.com) or [Jekyll](http://jekyllrb.com) had a baby
with [Django](http://djangoproject.com).
# The internet revolution

Internet is a revolution because of its purpose of being a communication
network for all Humans of the world. Communication is when a "message" is sent
by an "emiter" and delivered to a "reciever". We "all" have access to internet,
et we can freely access "all" pages, use and buy "all" goods and services for
sell on internet, as consumers.

## Free communication hardened by the consumer society

Howover, it's not that easy for any Human being to **freely** emit on internet.
We can all create pages on Facebook or SquareSpace but it doesn't belong to us.
Of course, this need for emiting messages on internet is so natural that
numerous are the persons who decided to make profit on that. In addition, these
services are each submitted to their own technical limitations, try to remove
the Facebook logo's on your Facebook page or to go far on SquareSpace without
paying and you'll see !

## "Django CMS Light, can you make a simple website for me please ?"

I'm learning to contribute to internet since 2004 and to contribute to the free
internet since 2007. It's one of my passions, sports, and also how I make a
decent living. Imagine how many persons have asked me to make a "simple
website" to communicate about their passions or their little business - which,
I hope, will merge more often than not. And how many times have I wanted to
help them but couldn't because I didn't have the time, I already take care of
so many websites and software already.

*I'm sure I'm not the only one with this issue.*

## Our mission

We're not only consumers, by facilitating internet website creation to our
local communities on internet, we greatly contribute to the development of free
communication between "all" Human being.

The purpose of Django CMS Light CMS is to be free, to belong to everybody. Every citizen
should know someone who knows what I'll call a "proximity hacker". I mean,
someone who's got a passion for the free internet and already contributes to
it, either professionaly, either for the sports, either for both.

Every hacker should be able to install Django CMS Light on his server somewhere, to allow
their friends to collaborate and contribute contents on their internet
websites. It's like wix or squarespace, perhaps a bit more sucking, because
it's made with less resources - but not necessarily less talent - but at least,
Django CMS Light belongs to everybody.
