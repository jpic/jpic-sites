Title: Pour rendre internet à l'Hummanité
Menu-title: Mission
Description: Developper la communication entre les Humains du monde et
             leur rendre la proprieté sur leurs contenus.

# La révolution d'internet

Internet est une véritable révolution de par sa vocation d'être un outil de
communication pour tous les êtres humains. La communication, c'est quand un
message est émis par un "émetteur" et reçu par un "recepteur".  Nous avons
"tous" accès à internet, et pouvons librement accèder à "toutes" les pages,
consommer et achter "tous" les biens et services mis en vente sur internet.

## La communication rendue difficile par l'ère de la consommation

Cependant, il n'est pas si simple pour tout être Humain de se placer
**librement** en tant qu'"émetteur". On peut faire des pages sur Facebook ou
SquareSpace, mais ça ne nous appartient pas. Bien sûr, ce besoin de communiquer
étant tellement naturel que bien des personnes ont eu l'idée d'en faire son
petit profit. En plus, c'est sujet à de nombreuses limitations techniques:
essayez de virer le logo Facebook de votre page facebook ou encore d'aller loin
avec SquareSpace sans payer et vous verrez.

## "Django CMS Light, tu peux me faire un site simple s'il-te-plait ?"

Voilà depuis 2004 que j'apprends à contribuer à internet et depuis 2007 à
l'internet libre, c'est l'une de mes passions, de mes sports, et également mon
gagne-pain. Imaginez combien de fois on a pu me demander un "site simple" pour
communiquer sur ses petites passions ou son petit business - qui, je l'éspère,
s'entre-mêlent de plus en plus souvent. Et combien de fois ai-je voulu aider
mon prochain mais, je n'avais tout simplement pas le temps. Pas le temps
d'installer un wordpress, qu'il faudrait que je maintienne et que je fasse
survivre dans le temps, tant il y a de sites internet dont je m'occuppe déjà.

*Je suis sûr que je ne suis pas le seul dans ce cas.*

## La mission de Django CMS Light

Nous ne sommes pas que des consomateurs. En facilitant la prise de rôle
d'"émetteur" à chacun sur internet, nous contribuons grandement au developpement
de la communication entre "tous" les êtres humains.

Le but de Django CMS Light est d'être libre, d'appartenir à tous. Tout citoyen devrait
connaitre ou connaître quelqu'un qui connait ce que j'appelerais un "hacker de
proximité", c'est à dire quelqu'un passionné par l'internet libre et qui y
contribue soit professionnelement, soit pour le sport, soit pour les deux.
Tout hacker doit pouvoir installer un Django CMS Light sur un serveur de son choix quelque
part, lui permettant de créer des comptes pour ses amis pour qu'ils puissent
ajouter des noms de domaines et collaborer ensemble pour leurs contenus.
C'est comme un wix ou squarespace, en un peu pourri peut-être, parce que fait
avec moyen de moyens - mais pas forcément moins de talents - mais au moins
Django CMS Light est à tout le monde.
