Menu-title: How ?
Description: DB provisions with contents of git branches for the urls this
             instance watches. Pushing commits on markdowns, js, css, yaml and images
             updates the websites.

# How ?

Django CMS Light is a multi-site
[CMS](https://en.wikipedia.org/wiki/Content_management_system) using [git
repositories](http://git-scm.org) to provision its database, stylesheet and
static contents. You can see the [stylesheet used on this
page](https://gitlab.com/jpic/jpic-sites/blob/master/page.css), the [background
image](https://gitlab.com/jpic/jpic-sites/blob/master/background.jpg) displayed
in responsive here, [this
text](https://gitlab.com/jpic/jpic-sites/blob/master/en.md) as well as its
[french translation](https://gitlab.com/jpic/jpic-sites/blob/master/fr.md), and
the [metadata used for this
website](https://gitlab.com/jpic/jpic-sites/blob/master/site.yaml).

Instead of generating static files, it's a WSGI server in
[python](http://python.org) and [django](http://djangoproject.com) which clones
[git repositories](https://gitlab.com/jpic/jpic-sites) parses their structure
and content to provision its database and cache. You don't have to use any web
form and the database is completely volatile. It's like
[Pelican](http://getpelican.com) or [Jekyll](http://jekyllrb.com) had a baby
with [Django](http://djangoproject.com).

*Django CMS Light is dedicated to all the small websites we never have time to
do for our friends.*

