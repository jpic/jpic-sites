Menu-title: Comment ?
Description: La DB se provisionne avec les branches des URLs git de cette
             instance. Il suffit de commiter des markdowns, js, css, yaml et
             images pour modifier les sites.

# Comment fonctionne Django CMS Light

Django CMS Light est un [système de gestion de
contenu](https://fr.wikipedia.org/wiki/Syst%C3%A8me_de_gestion_de_contenu)
multi-site qui utilise des [dépots git](http://git-scm.org) pour provisionner
sa base de données, feuilles de styles et contenus multimédias, vous pouvez
voir la [feuille de style utilisée sur cette
page](https://gitlab.com/jpic/jpic-sites/blob/master/page.css), l'[image de
fond](https://gitlab.com/jpic/jpic-sites/blob/master/background.jpg) affichée
en responsive, [ce texte](https://gitlab.com/jpic/jpic-sites/blob/master/fr.md)
ansi que sa [traduction
anglaise](https://gitlab.com/jpic/jpic-sites/blob/master/en.md), et enfin les
[meta-données du
site](https://gitlab.com/jpic/jpic-sites/blob/master/site.yaml) qui définissent
des variables.

Au lieu de générer des fichiers statiques, c'est un serveur en
[python](http://python.org) et [django](http://djangoproject.com) qui clône
des [dépôts git](https://gitlab.com/jpic/jpic-sites) et lit leurs structures et
contenus pour provisionner sa base de données et son cache, n'ayant à faire à
aucun formulaire HTML ni en n'ayant aucune donnée à persister en base de données.
C'est un peu comme la rencontre de la philosophie de
[Pelican](http://getpelican.com) ou [Jekyll](http://jekyllrb.com) et
[Django](http://djangoproject.com).

*Django CMS Light est dédié à tous les petits sites que
nous n'avons jamais le temps de faire pour nos amis.*
