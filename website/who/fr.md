Menu-title: Qui ?
Url: qui

# Pour étudiants à vie

Ré-écriture #4233 d'un projet de CMS en Django dans le cadre d'une experience
de developpement d'économie locale en tant qu'outil de communication libre pour
tous.

## Merci

- Linux,
- Python,
- PostgreSQL,
- Nginx,
- Django,
- Pinax,
- Bootstrap,
- LessCSS,
