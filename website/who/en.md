Menu-title: Who ?
Url: who

# For lifetime students

Rewrite #4233 of a CMS project in Django as part of an experiment on developing
local economy using internet as the free communication tool for all.

## Thanks

- Linux,
- Python,
- PostgreSQL,
- Nginx,
- Django,
- Pinax,
- Bootstrap,
- LessCSS,
